/**
 * TCD Software
 * Created by Dmitrij Rysanow on 12.12.16.
 */

/**
 * Dependencies
 */
const gulp = require("gulp"),
    nwBuilder = require("nw-builder"),
    fs = require("fs"),
    path = require("path"),
    runSequence = require("run-sequence").use(gulp),
    exec = require("child_process").exec,
    spawn = require("child_process").spawn,
    log = require("npmlog"),
    argv = require("yargs").argv,
    extend = require("extend"),
    currentPlatform = require('nw-builder/lib/detectCurrentPlatform.js'),
    backend = require("backend-rustreamer");


const availablePlatforms = ['linux32', 'linux64', 'win32', 'osx64'];


/**
 * returns an array of platforms that should be built
 * @returns {Array<string>} platform to be built
 */
const parsePlatforms = () => {
    const requestedPlatforms = (argv.platforms || currentPlatform()).split(','),
        validPlatforms = [];

    for (let i in requestedPlatforms) {
        if (availablePlatforms.indexOf(requestedPlatforms[i]) !== -1) {
            validPlatforms.push(requestedPlatforms[i]);
        }
    }

    // for win, 32-bits works on 64, if needed
    if (availablePlatforms.indexOf('win64') === -1 && requestedPlatforms.indexOf('win64') !== -1) {
        validPlatforms.push('win32');
    }

    // remove duplicates
    validPlatforms.filter((item, pos) => {
        return validPlatforms.indexOf(item) === pos;
    });

    return requestedPlatforms[0] === 'all' ? availablePlatforms : validPlatforms;
};

/**
 * Config for building nwjs app
 * @type {{files: string, flavor: string, buildDir: string, zip: boolean, version: string, withFFmpeg: boolean, platforms: string[], downloadUrl: string}}
 */
const BUILD_DEFAULTS = {
    files: "./node_modules/ui-rustreamer/**/**",
    flavor: 'sdk',
    buildDir: "build",
    zip: false,
    version: "0.18.6",
    withFFmpeg: true,
    platforms: ['linux64', 'win32', 'win64', 'osx64'],
    downloadUrl: 'http://builds.butterproject.org/nw/'
};

const getBuildConfig = () => {
    return extend(BUILD_DEFAULTS, argv);
};
var config = getBuildConfig();
if (typeof config.platforms === "string") {
    config.platforms = [config.platforms];
}

const parseReqDeps = () => {
    return new Promise((resolve, reject) => {
        exec('npm ls --parseable=true --production=true', { maxBuffer: 1024 * 500 }, (error, stdout, stderr) => {
            if (error || stderr) {
                reject(error || stderr);
            } else {
                // build array
                let npmList = stdout.split('\n');

                // remove empty or soon-to-be empty
                npmList = npmList.filter((line) => {
                    return line.replace(process.cwd().toString(), '');
                });

                // format for nw-builder
                npmList = npmList.map((line) => {
                    return line.replace(process.cwd(), '.') + '/**';
                });

                // return
                resolve(npmList);
            }
        });
    });
};

const attachBuildConfig = () => {
    log.info("Attaching backend-rustreamer config");
    let attachedConfig = {
        "hostFront": false,
        "frontDir": "node_modules/ui-rustreamer",
        "port": "7000",
        "debug": false,
        "ApiEndpoint": "http://api.rutracker.org:80",
        "FeedEndpoint": "http://feed.rutracker.org/atom/f/1880.atom",
        "run": false
    };
    log.info(JSON.stringify(attachedConfig));
    fs.writeFileSync('build/Ruflix/' + config.platforms[0] + '/node_modules/backend-rustreamer/config.json', JSON.stringify(attachedConfig));
};

gulp.task('clean', function () {
    var clean = require('gulp-clean');
    return gulp.src(['build'])
        .pipe(clean({ force: true }));
});

/**
 * Gulp tasks manual.
 */
gulp.task('default', () => {
    console.log([
        '\nBasic usage:',
        ' gulp run\tStart the application in dev mode',
        ' gulp build\tBuild the application',
        ' gulp server\tStart dev server and develop in Chrome',
        '\nAvailable options:',
        ' --platforms=<platform>',
        '\tArguments: ' + "linux64" + ',all',
        '\tExample:   `gulp build --platforms=all`',
        '\nUse `gulp --tasks` to show the task dependency tree of gulpfile.js\n'
    ].join('\n'));
});

/**
 * Download and compile nwjs, used by Gulp Build
 */
gulp.task('nwjs', () => {
    return parseReqDeps().then((requiredDeps) => {
        var nw = new nwBuilder(config).on('log', log.info);
        nw.options.files = ['./package.json', './README.md', './node_modules/**'];
        // add node_modules
        nw.options.files = nw.options.files.concat(requiredDeps);
        // remove junk files
        nw.options.files = nw.options.files.concat(['!./node_modules/**/*.bin', '!./node_modules/**/*.c', '!./node_modules/**/*.h', '!./node_modules/**/Makefile', '!./node_modules/**/*.h', '!./**/test*/**', '!./**/doc*/**', '!./**/example*/**', '!./**/demo*/**', '!./**/bin/**', '!./**/.*/**']);
        return nw.build();
    }).then(attachBuildConfig).catch(console.log.bind(console));

});

gulp.task('build', () => {
    log.enableProgress();
    runSequence('clean', 'nwjs', () => {
        log.disableProgress();
    });
});

gulp.task('run', () => {
    return new Promise((resolve, reject) => {
        let platform = parsePlatforms()[0],
            bin = path.join('cache', config.version + '-sdk', platform);
        // path to nw binary
        switch (platform.slice(0, 3)) {
            case 'osx':
                bin += '/nwjs.app/Contents/MacOS/nwjs';
                break;
            case 'lin':
                bin += '/nw';
                break;
            case 'win':
                bin += '/nw.exe';
                break;
            default:
                reject(new Error('Unsupported %s platform', platform));
        }

        console.log('Running %s from cache', platform);

        // spawn cached binary with package.json, toggle dev flag
        const child = spawn(bin, ['.', '--development']);

        // nwjs console speaks to stderr
        child.stderr.on('data', (buf) => {
            console.log(buf.toString());
        });

        child.on('close', (exitCode) => {
            console.log('%s exited with code %d', pkJson.name, exitCode);
            resolve();
        });

        child.on('error', (error) => {
            // nw binary most probably missing
            if (error.code === 'ENOENT') {
                console.log('%s is not available in cache. Try running `gulp build` beforehand', platform);
            }
            reject(error);
        });
    });
});

/**
 * Runs backend with front hosted
 */
gulp.task('serve', () => {
    return backend.run({
        hostFront: true,
        frontDir: path.resolve(__dirname, 'node_modules/ui-rustreamer')
    });
});